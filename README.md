# 1. name:  alta3research-ansible-cert

# 2. Desctiption: Ansible 101 certification

# playbook file name: alta3research-ansiblecert01.yml
# This playbook is to upgrade kernels in 20k of our Linux servers (Oracle Linux; it is a Redhat compatible Linux)
# There are 2 types of kernels for this OS: RHCK and UEK.
# How we know which kernel should we install or upgrade ?
# If it is a docker server (typically, there is a package named docker-engine), it should be running in UEK
# ie. if rpm -qa|grep docker-engine is true, then install and running UEK
# Or else it should be running with RHCK
# Not all servers have python installed by default
# The script for UEK upgrade is from http://linux-build-http.us.dell.com/images/linux/kernel/patch_uek.sh
# The script for RHCK upgrade is from http://linux-build-http.us.dell.com/images/linux/kernel/patch_rhck.sh
# we need to wget upgrade script and run locally from /tmp
# For test purpose, I use 'localhost' instead here

# 3. Demo scripts:

patch_rhck.sh

#!/bin/bash
date >>/tmp/patch.log
echo "No docker/container service detected. Proceed RHCK upgrade." >>/tmp/patch.log

patch_uek.sh

#!/bin/bash
date >>/tmp/patch.log
echo "Docker/container service detected. Proceed UEK upgrade." >>/tmp/patch.log

# 4. Ansible-playbook execution output example:

# ansible-playbook /opt/ans/alta3research-ansiblecert01.yml

PLAY [Ansible 101 certification]

TASK [Gathering Facts]
ok: [localhost]

TASK [Install py3 and pip3 with ansible raw]
changed: [localhost]

TASK [Gather all packages's information]
ok: [localhost]

TASK [For docker servers - upgrade and stage UEK]
skipping: [localhost]

TASK [For none docker servers - upgrade and stage RHCK]
ok: [localhost]

TASK [Run kernel upgrade from local]
changed: [localhost]

PLAY RECAP **
localhost                  : ok=5    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0

# 5. Execution result:

# cat /tmp/patch.log
Sun Dec 19 03:16:01 CST 2021
No docker/container service detected. Proceed RHCK upgrade.

### verified as expected

